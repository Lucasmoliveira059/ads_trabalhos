import java.util.Scanner;

public class HelloWorld {

	public static void main(String[] args) {
		try (Scanner input = new Scanner(System.in)) {
			System.out.println("Digite seu nome: ");
			String nome = input.next();
			System.out.println("Ol�, seu nome �: " + nome);
		}
	}
}
