package oop01;

public class TesteVeiculo {
	
	public static void main(String[] args) {

	Veiculos carro01 = new Veiculos();
	carro01.setModelo("500");
	carro01.setAno(2022);
	carro01.setKm(0);
	carro01.setPreco(70000);
	carro01.setMarca("Fiat");
	carro01.setCor("Vermelho");
	System.out.println( carro01.toString() );
	
	Veiculos carro02 = new Veiculos("Fiesta", 2012, 40000, 17000, "Ford", "Verde");
	System.out.println( carro02.toString() );
	}
}
