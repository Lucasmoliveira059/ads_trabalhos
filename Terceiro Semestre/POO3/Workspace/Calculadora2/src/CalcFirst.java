import java.awt.Font;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class CalcFirst extends JFrame {
	
	private double valorTotal = 0;
	private String operador   = "";
	private JPanel pnlDisplay = null;
	private JPanel pnlTeclado = null;
	private JButton[] buttons = null;
	private JTextField display = null;
	private boolean zeraDisplay = false;
	
	public static void main(String[] args) {
		new CalcFirst();
	}
	
	public CalcFirst() {
		
		this.setBounds(5, 5, 400, 600);
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		this.setTitle("Calculadora");
		this.setLayout(null);
		
		this.pnlDisplay = new JPanel();
		pnlDisplay.setBounds(5, 5, 390, 100);
		pnlDisplay.setLayout(null);
		this.add( pnlDisplay );
		
		this.display = new JTextField();
		display.setHorizontalAlignment( SwingConstants.RIGHT );
		display.setBounds( 0 , 15, 390, 60);
		pnlDisplay.add( display );
		
		Font displayFont = new Font( "Courier New", Font.BOLD, 30); 
		display.setFont( displayFont );
		
		this.pnlTeclado = new JPanel();
		int x = 5;
		int y = pnlDisplay.getY() + pnlDisplay.getHeight()+ 5;
		int w = pnlDisplay.getWidth();
		int h = this.getHeight() - pnlDisplay.getHeight() - 15;
		pnlTeclado.setBounds( x, y, w, h);
		pnlTeclado.setLayout(null);
		this.add(pnlTeclado);
		
		int btnsize = (this.getWidth() - 30 ) / 4;
		
		String[]  labels  = new String[] 
				{
					"7","8","9","/",
					"4","5","6","*",
					"1","2","3","-",
					"0",".","=","+",
				} ;
		String operadores =  "/*-+=" ;
		this.buttons = new JButton[16];
		
		int i = 0;
		for ( i = 0; i <= 15 ; i++) {
			buttons[i] = new JButton( "" + labels[i] );
			buttons[i].setFont(displayFont);
			pnlTeclado.add(buttons[i]);
			
			buttons[i].addActionListener( new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String btnLabel = ((JButton) e.getSource()).getLabel();
					if ( ! operadores.contains(btnLabel) ) {
						if ( ! btnLabel.equals(".") ) {
							if ( zeraDisplay) {
								display.setText("");
								zeraDisplay = false;
							}
							display.setText( display.getText() +  btnLabel );
						} else {
							if ( ! display.getText().contains(".") ){
								display.setText( display.getText() +  btnLabel );
							}
						}
					} else {
						acumulaOperador(btnLabel);
					}
				}
			} );
		}
		
		i =0;
		for(int linha = 0; linha < 4; linha++ ) {
			for(int coluna = 0; coluna < 4; coluna++ ) {
				buttons[i].setBounds( btnsize * coluna + ((coluna + 1) * 05), btnsize * linha + ((linha + 1) * 05), btnsize, btnsize);
				i++;
			}	
		}
		

		
		this.setVisible(true);
	}
	
	private void acumulaOperador( String operador ) {
		if ( operador.equals("=")) {
			this.igual();
		}else {
			if ( ! this.operador.equals("")) {
				this.igual();
			}
			
			this.setOperador(operador);
			this.setValorTotal( new Double( this.display.getText()));
			this.zeraDisplay = true;
		}

	}
	
	@SuppressWarnings("removal")
	private void igual() {
		if ( ! this.getOperador().equals("") ) {
			double valor = 0;
			switch ( this.getOperador() ){
				case "+": {
					valor = getValorTotal() + new Double( this.display.getText());
				} break;
				case "-": {
					valor = getValorTotal() - new Double( this.display.getText());
				} break;
				case "*": {
					valor = getValorTotal() * new Double( this.display.getText());
				} break;
				case "/": {
					valor = getValorTotal() / new Double( this.display.getText());
				} break;
			}
			this.display.setText( new Double(valor).toString() );
			this.setValorTotal( 0 );
			this.setOperador("");
		} 
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}
}
