package POO3;
import java.util.Scanner;

public class Exercicio20 {

	public void run() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor em metros: \n");
		float a = input.nextFloat();
		
		float p = a *3315;
		
		if(a < 0) {
			System.out.print("a operação de conversão não pode ser efetuada. \n");
		}else {
			System.out.println("O valor em Pés é de "+ p);
		}
		
	}
}
