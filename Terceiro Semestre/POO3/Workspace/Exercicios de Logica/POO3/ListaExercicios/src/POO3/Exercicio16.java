package POO3;
import java.util.Scanner;

public class Exercicio16 {

	public void run() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite a base: \n");
		float base = input.nextFloat();
		
		System.out.print("Digite a altura: \n");
		float altura = input.nextFloat();
		
		float area = (base*altura)/2;
		
		System.out.print("Area do Triangulo igual a: "+area);

		
	}
}
