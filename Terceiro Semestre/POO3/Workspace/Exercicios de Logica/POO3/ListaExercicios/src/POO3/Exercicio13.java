package POO3;
import java.util.Scanner;

public class Exercicio13 {

	public void run() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite o valor A: \n");
		float a = input.nextFloat();
		
		System.out.print("Digite o valor B: \n");
		float b = input.nextFloat();
		
		System.out.print("Digite o valor C: \n");
		float c = input.nextFloat();
		
		if( a < b+c && b < a+c && c < a+b) {
			String triangulo;
			
			if((a==b)&& (b==c)&&(c==a)) {
				triangulo = "equilatero";
			}
			if((a==b)!= (b==c)!=(c==a)) {
				triangulo = "escaleno";
			}
			else {
				triangulo = "isoceles";
			}
			System.out.print("\nEste conjunto de medidas formam um triângulo " + triangulo);
			
		}else {
			System.out.print("\nEste conjunto de medidas não formam um triângulo.");
		
		}
	}
}
