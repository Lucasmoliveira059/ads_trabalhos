package POO3;
import java.util.Scanner;

public class Exercicio27 {

	public void run() {
		float n=0, media=0, notas=0,c=0;

		Scanner input = new Scanner(System.in);
		
		do {
		System.out.print("Digite a(s) nota(s) da(o) aluna(o) ou um número negativo para encerrar o programa ");
		n = input.nextFloat();
		c++;
		
		if(n >= 0) {
			notas = n+notas;
		}
		
		}while(n >= 0);
		
		media = (notas/(c-1));
		
		System.out.print("\nA nota final foi: "+media);
		System.out.print("\nA média foi calculada em cima de : "+ (c-1) +" notas");		
	}
}
