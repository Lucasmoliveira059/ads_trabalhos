package POO3;
import java.util.Scanner;

public class Exercicio15 {

	public void run() {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite o valor de X: ");
		int x = input.nextInt();
		System.out.print("Digite o valor de Y: ");
		int y = input.nextInt();
		
		System.out.println("A soma dos valores x e y é: "+ (x+y));
		System.out.println("Produto: " + (x * y));
		
		if(x > y) {
			System.out.println("O valor "+ (x) +"É maior que "+ (y));
		}
		else if(x < y) {
			System.out.println("O valor "+ (x) +"É menor que "+ (y));
		}
		else {
			System.out.println("O valor "+ (x) +"É igual ao de "+ (y));
		}
		
		
	}
}
