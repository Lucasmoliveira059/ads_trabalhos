package POO3;
import java.util.Scanner;

public class Exercicio14 {

	public void run() {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite a primeira nota: \n");
		float a = input.nextFloat();
		System.out.print("Digite a segunda nota: \n");
		float b = input.nextFloat();
		System.out.print("Digite a terceira nota: \n");
		float c = input.nextFloat();
		
		float media;
		media = (a + b + c) / 3; 
		
		if(media>7){
			System.out.println("\nNota: "+ media);
			System.out.println("\nAluno Aprovado!\n");
			}
		 else if(media>3 && media<7) {
			System.out.println("\nNota: "+ media);
			System.out.println("\nExame!\n");
		 	}
		else if(media<3) {
			System.out.println("\nNota: "+ media);
			System.out.println("\nReprovado!\n");
			}
	}
}
