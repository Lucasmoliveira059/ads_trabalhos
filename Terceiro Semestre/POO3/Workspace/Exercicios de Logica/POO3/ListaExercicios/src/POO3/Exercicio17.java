package POO3;
import java.util.Scanner;

public class Exercicio17 {

	public void run() {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Digite um valor em segundos para serem convertidos em Horas: \n");
		float seg = input.nextFloat();
		
		float hora = seg/3600;
		
		System.out.println("total de "+hora+" Horas");

		if(seg<0) {
		
			System.out.println("Operação negada");
		}
	}
}