package POO3;
import java.util.Scanner;

public class Exercicio24 {

	public void run() {
		Scanner input = new Scanner(System.in);
		System.out.print("Digite um numero inteiro entre 1 e 20: ");
		int num = input.nextInt();
		int a = 1;
		
		if((num <= 20) && (num > 0) ) {
			System.out.println("\n");
			
			for(a = 1; a <=10; a++) {
				System.out.println(num+" X "+ a+": "+(num*a));
			}
			
			a=1;
			System.out.println("\n");
	
			while( a <= 10) {
				System.out.println(num+" X "+ a+": "+(num*a));
				a++;
			}
			
			a=1;
			System.out.println("\n");
	
			do {
				System.out.println(num+" X "+ a+": "+(num*a));
				a++;
			}while(a <= 10);
		}else {
			System.out.println("Opera��o inv�lida!");
		}
	}
}
