create database Survey;

use Survey;

create table salary_survey(
	`country` char(30),
    `years_experience` int,
    `employment_status` char(50),
    `job_title` char(50),
    `is_manager` char(50),
    `education_level` char(50)
)

select * from salary_survey;