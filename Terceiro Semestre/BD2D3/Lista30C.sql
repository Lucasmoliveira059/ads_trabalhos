USE `classicmodels`;

select count(*) from customers;

select count(*) from customers
where contactFirstName = 'Christina';

select SUM(employeeNumber) from employees
where employeeNumber > 1400;

select lastName from employees
group by lastName
having lastName = 'Patterson';

select min(priceEach) from orderdetails;

select avg(priceEach) from orderdetails;

select count(*) from orderdetails o 
join products p on o.productCode = p.productCode
where o.productCode  = 'S12_1099';

select firstName from employees;

select count(*) from employees e
join offices o
group by e.lastName, e.firstName, o.officeCode
having officeCode < 150;

-- selecionando através de subqueries não relacionadas
select contactFirstName, contactLastName, city from customers
where customerNumber in
(select customerNumber from customers
where customerNumber > 200);

-- selecionando através de subqueries relacionais
select orderNumber,
(select count(orderNumber) from orderdetails
where orderNumber = o.orderNumber)
from orders o;

select count(officeCode) from offices;

select count(*) from offices
where city = 'Paris';

select SUM(employeeNumber) from employees
where employeeNumber < 1400;

select jobTitle,employeeNumber from employees
group by employeeNumber
having jobTitle = 'Sales Rep';

select min(customerNumber) from orders;

select avg(customerNumber) from orders;

select count(*) from orders o 
join customers c on o.customerNumber = c.customerNumber
where o.customerNumber  = 129;

select lastName from employees;

select firstName, lastName, email from employees
where officeCode in
(select officeCode from employees
where officeCode = 2);

select officeCode,
(select count(officeCode) from employees
where officeCode = e.officeCode)
from employees e;

select count(*) from products;

select count(*) from products
where productLine = 'Motorcycles';

select SUM(buyPrice) from products
where buyPrice > 100;

select paymentDate from payments
group by paymentDate
having paymentDate = '2004-10-19';

select min(amount) from payments;

select avg(amount) from payments;

select count(*) from payments p
join customers c on c.customerNumber = p.customerNumber
where c.customerNumber  = 103;

select checkNumber from payments;

select customerNumber, checkNumber from payments
where customerNumber in
(select customerNumber from payments
where customerNumber > 150);

select customerName,
(select count(customerName) from customers
where customerName = c.customerName)
from customers c;