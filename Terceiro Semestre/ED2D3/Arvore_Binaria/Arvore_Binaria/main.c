#include <stdio.h>
#include <stdlib.h>
#include "ArvBin.h"
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Portuguese");

    int x;
    ArvBin *raiz;

    raiz = cria_arvBin();

    if(vazia_arvBin(raiz)){
        printf("A �rvore est� vazia! ");
    } else{ printf("A �rvore possui elementos");}
    printf("\n");

    x = altura_arvBin(raiz);
    printf("A altura da �rvore �: %d", x);

    x = totalNO_arvBin(raiz);
    printf("O Total de n�s  na �rvore �: %d", x);

    preOrdem_arvBin(raiz);
    emOrdem_arvBin(raiz);
    posOrdem_arvBin(raiz);

    x = insere_arvBin(raiz, 150);
    x = insere_arvBin(raiz, 110);
    x = insere_arvBin(raiz, 100);
    x = insere_arvBin(raiz, 130);
    x = insere_arvBin(raiz, 120);
    x = insere_arvBin(raiz, 140);
    x = insere_arvBin(raiz, 160);
}
