#include <stdio.h>
#include <stdlib.h>
#include "ArvBin.h"

struct NO {

    int info;
    struct NO *esq;
    struct NO *dir;
};

ArvBin *cria_arvBin(){

    ArvBin *raiz = (ArvBin*) malloc(sizeof(ArvBin));
    if(raiz != NULL){
        *raiz = NULL;
    }
};

void liberar_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return;
    }
    libera_NO(*raiz);
    free(raiz);
};

void libera_NO(struct NO *no){

    if(no == NULL){
        return;
    }
    libera_NO(no->esq);
    libera_NO(no->dir);
    free(no);
    no = NULL;
};

int vazia_arvBin (ArvBin *raiz){

    if(raiz == NULL){
        return 1;
    }
    return 0;
};

int altura_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = altura_arvBin(&((*raiz)->esq));
    int alt_dir = altura_arvBin(&((*raiz)->dir));
    if(alt_esq > alt_dir){
        return(alt_esq +1);
    }
    else{ return(alt_dir +1);}

};

int totalNO_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = totalNO_arvBin(&((*raiz)->esq));
    int alt_dir = totalNO_arvBin(&((*raiz)->dir));
    return(alt_esq + alt_dir +1);
};

void preOrdem_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return;
    }
    if(*raiz != NULL){
        printf("%d \n", (*raiz)->info);
        preOrdem_arvBin(&((*raiz)->esq));
        preOrdem_arvBin(&((*raiz)->dir));
    }
};

void emOrdem_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return;
    }
    if(*raiz != NULL){
        emOrdem_arvBin(&((*raiz)->esq));
        printf("%d \n", (*raiz)->info);
        emOrdem_arvBin(&((*raiz)->dir));
    }
};

void posOrdem_arvBin(ArvBin *raiz){

    if(raiz == NULL){
        return;
    }
    if(*raiz != NULL){
        posOrdem_arvBin(&((*raiz)->esq));
        posOrdem_arvBin(&((*raiz)->dir));
        printf("%d \n", (*raiz)->info);
    }
};

int insere_arvBin(ArvBin *raiz, int valor){

    if(raiz == NULL){
        return;
    }
    struct NO *novo;
    novo = (struct NO*) malloc(sizeof(struct NO));
    if(novo == NULL){
        return 0;
    }
    novo->info = valor;
    novo->dir = NULL;
    novo->esq = NULL;
    if(*raiz == NULL){
        *raiz = novo;
    } else{
        struct NO *atual = *raiz;
        struct NO *ant = NULL;
        while(atual != NULL){
            ant = atual;
            if(valor == atual->info){
                free(novo);
                return 0;}
            if(valor > atual->info){
                atual = atual->dir;
                }else{atual = atual->esq;}
            if(valor > ant->info){
                ant->dir = novo;
                }else{ant->esq = novo;}
            }
        return 1;
        }
};
